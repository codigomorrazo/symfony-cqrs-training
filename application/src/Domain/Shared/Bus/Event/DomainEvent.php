<?php

namespace App\Domain\Shared\Bus\Event;

use ReflectionClass;

abstract class DomainEvent
{
    public function __toString(): string
    {
        $className = (new ReflectionClass($this))->getShortName();

        return sprintf(
            'Handling "%s" with values (%s)',
            $className,
            json_encode($this->getMessageAttributes())
        );
    }

    protected function getMessageAttributes(): array
    {
        return get_object_vars($this);
    }
}
