<?php

declare(strict_types = 1);

namespace App\Tests\Infrastructure\Controller\BusTest;

use App\Application\Shared\Bus\Query\QueryBus;
use App\Infrastructure\Controller\BusTest\BusTestController;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

final class BusTestControllerTest extends TestCase
{
    private BusTestController $controller;
    private MockObject|QueryBus $queryBus;

    protected function setUp(): void
    {
        $this->queryBus = $this->createMock(QueryBus::class);
        $this->controller = new BusTestController($this->queryBus);
    }

    public function testControllerMustThrowErrorWhenQueryBusFails(): void
    {
        $this->expectException(Exception::class);
        $this->queryBus->method('execute')->willThrowException(new Exception());

        $this->controller->__invoke();
    }

    public function testExpectAnOkResponseWithDataContentWhenNothingFails(): void
    {
        $this->queryBus->method('execute')->willReturn([]);
        $response = $this->controller->__invoke();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode());
        self::assertArrayHasKey('data', json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR));
    }
}

