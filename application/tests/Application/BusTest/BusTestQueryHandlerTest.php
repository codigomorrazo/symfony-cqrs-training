<?php

declare(strict_types = 1);

namespace App\Tests\Application\BusTest;

use App\Application\BusTest\BusTestQuery;
use App\Application\BusTest\BusTestQueryHandler;
use PHPUnit\Framework\TestCase;

final class BusTestQueryHandlerTest extends TestCase
{
    public function testResponseMustBeEmptyArrayGivenBusTestQueryWhenHandlerOutputs(): void
    {
        $handler = new BusTestQueryHandler();
        $response = $handler->__invoke(new BusTestQuery());

        self::assertSame([], $response);
    }
}

